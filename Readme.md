This is the repository for the android application of Get Fit Buddy. Built with Ionic and Firebase

DO

git clone https://priyadarsan@bitbucket.org/priyadarsan/getfitbuddy.git

npm install

FOR ANDROID: ionic cordova run android

FOR IOS: ionic cordova run ios

NOTE: YOU MUST HAVE AN EMULATOR IN YOUR MACHINE OR AN PHYSICAL ANDROID OR IOS DEVICE TO RUN THIS PROJECT. INSTALL ANDROID STUDIO TO HAVE AN EMULATOR. YOU SHOULD HAVE A MAC TO RUN THE IOS VERSION

Copyrights Priyadarsan Mahendiran